from flask import Flask,jsonify,Blueprint,request
from flask_restplus import Api, Resource,fields
from sqlalchemy import Column, Integer, Unicode, UnicodeText, String
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
# from marshmallow import Schema, fields !!!, pprint, post_load
from marshmallow import Schema, pprint, post_load
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow


#https://stackoverflow.com/questions/32477878/flask-restplus-route

engine = create_engine('sqlite:///lits.db', echo=True)
Base = declarative_base(bind=engine)


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///lits.db'
db = SQLAlchemy(app)
ma = Marshmallow(app)
blueprint = Blueprint('api', __name__, url_prefix='/api')

api = Api(blueprint,description="hostedby API")
ns = api.namespace('phpversion', description='Operations related to phpVersion')
ns2 = api.namespace('customerACL', description='Operations related to customerACL')


app.register_blueprint(blueprint)
    #Base.metadata.bind = engine
    #Session = sessionmaker(bind=engine)
    #s= Session()
s=db.Session(bind=engine)

resource_fields = api.model('PhpVersion post', {
  'idPhpVersion' :  fields.Integer(required=True, description='Article title'),
  'displayName' :  fields.String(required=True, description='Article title'),
  'localIP' : fields.String(required=True, description='Article title'),
  'fpmPort' : fields.Integer(required=True, description='The unique identifier of a blog post'),
  'fpmCachePort' : fields.Integer(required=True, description='The unique identifier of a blog post'),
}
)
resource_fields2 = api.model('PhpVersion pop', {
  'displayName' :  fields.String(required=True, description='Article title'),
  'localIP' : fields.String(required=True, description='Article title'),
  'fpmPort' : fields.Integer(required=True, description='The unique identifier of a blog post'),
  'fpmCachePort' : fields.Integer(required=True, description='The unique identifier of a blog post'),
}
)


class PhpVersion(db.Model):
    __tablename__ = 'PhpVersion'
    idPhpVersion = db.Column(Integer, primary_key=True)
    displayName = db.Column(String(45))
    localIP = db.Column(String(32))
    fpmPort = db.Column(Integer)
    fpmCachePort = db.Column(Integer)

    def __init__(self):
        idPhpVersion = self.idPhpVersion
        displayName = self.displayName
        localIP = self.localIP
        fpmPort = self.fpmPort
        fpmCachePort = self.fpmCachePort

    def __repr__(self):
        return 'idPhpVersion: {}'.format(self.idPhpVersion)

class PhpVersionSchema(ma.Schema):
    class Meta:
        model = PhpVersion
        fields = ('idPhpVersion','displayName','localIP','fpmPort','fpmCachePort')

@ns2.route('/')
class dd(Resource):
    def get(self):
        """GET phpVersion List"""
        phps = s.query(PhpVersion).all()
        schema = PhpVersionSchema(many=True)
        result = schema.dump(phps)
        return jsonify(result.data)

@ns.route('/')
class PhpVersionCollection(Resource):
    def get(self):
        """GET phpVersion List"""
        phps = s.query(PhpVersion).all()
        schema = PhpVersionSchema(many=True)
        result = schema.dump(phps)
        return jsonify(result.data)

    @api.response(201, 'Blog post successfully created.')
    @api.expect(resource_fields)
    def post(self):
        """Costam costam"""
        pp=PhpVersion()
        data = request.json
        pp.idPhpVersion=data.get('idPhpVersion')
        pp.displayName=data.get('displayName')
        pp.localIP=data.get('localIP')
        pp.fpmPort=data.get('fpmPort')
        pp.fpmCachePort=data.get('fpmCachePort')
        s.add(pp)
        s.commit()
        return None, 201



@ns.route('/<int:id>')
class PhpVersionItem(Resource):
    def get(self,id):
        """GET Costam costam"""
        #Base.metadata.bind = engine
        #Session = sessionmaker(bind=engine)
        #s= Session()
        pp= s.query(PhpVersion).filter(PhpVersion.idPhpVersion == id).one()
        schema = PhpVersionSchema()
        result = schema.dump(pp)
        return jsonify(result.data)



### delete
#https://www.pythoncentral.io/understanding-python-sqlalchemy-session/
    @api.response(201, 'Successfully deleted.')
    def delete(self,id):
        """Delete"""
        pp= s.query(PhpVersion).filter(PhpVersion.idPhpVersion == id).one()
        s.delete(pp)
        s.commit()
        return None, 201


##put
    @api.response(201, 'Blog post successfully created.')
    @api.expect(resource_fields2)
    def put(self,id):
        """udpate"""
        pp= s.query(PhpVersion).filter(PhpVersion.idPhpVersion == id).one()
        data = request.json
        pp.displayName=data.get('displayName')
        s.commit()
        return None, 201

if __name__ == '__main__':
    app.run(debug=True)
