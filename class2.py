import sqlite3

class Atari:
    model='800XL'


    @classmethod
    def setModel(cls,model):
        cls.model='130XE'


class Person(object):
    def __init__(self,name):
      self.name = name

    def __repr__(self):
      return 'Name: {}'.format(self.name)


def main():
    Atari.setModel("123")
    atari = Atari()
    print("Model: %s" % (atari.model))

    person = Person("Marek")
    print(person)

    conn = sqlite3.connect("lits.db")
    


if __name__ == '__main__': main()
