from marshmallow import Schema, fields, pprint, post_load



class Person(object):
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def __repr__(self):
        return '{} is {} years old'.format(self.name, self.age)

class PersonSchema(Schema):
    name = fields.String()
    age = fields.Integer()

    @post_load
    def create_person(self, data):
        return Person(**data)

input_dict = {}

input_dict['name'] = 'Jan'
input_dict['age'] = 12

person = Person(name=input_dict['name'], age=input_dict['age'])

schema = PersonSchema()
result = schema.dump(person)
#result = schema.load(input_dict)

pprint(result.data)
