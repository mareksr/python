#!/bin/python


from __future__ import print_function
import os
import re
import sys
import MySQLdb
from dateutil import parser


def main():
  db = MySQLdb.connect('','','',"")
  x = db.cursor(MySQLdb.cursors.DictCursor)

  res = loadText()
  for line in res:
    match=re.search(r"^(.*?)\s+\"\"\s+(.*?)\s+(.*20.*?)\s+\s+(.*?)$", line)
    if match:
      try:
        idServiceWWW=getIdServiceWWW(x,match.group(1))
        if idServiceWWW is None:
          idServiceWWW=1
 
        idCustomer=getIdCustomer(x,idServiceWWW)
#sprawdz czy juz wpis jest w bazie
        idSsl=check(x,match.group(1))
        if idSsl is None:
          x.execute("""INSERT INTO `sslService` (idCustomer,mainDomain,SANDomains,created,renew,serverProxy) 
            VALUES (%s,%s,%s,%s,%s,%s)""",(idCustomer,match.group(1),match.group(2),parser.parse(match.group(3)),
            parser.parse(match.group(4)),"xproxy2"))
        else:
          x.execute("""UPDATE `sslService` SET created=%s, renew=%s WHERE idSsl=%s""",(parser.parse(match.group(3)), parser.parse(match.group(4)),idSsl))
        db.commit()
#zrob update dat
          
   
      except Exception, e:
        print ("1:%s"%(e))
        db.rollback()
  db.close()



def check(x,domain):
  try:
     x.execute("""SELECT `idSsl` FROM `sslService` WHERE mainDomain=%s""",[domain])
     results = x.fetchall()
     for row in results:
       bname = row["idSsl"]
       return bname
  except Exception, e:
     print ("Exception:%s" % (e))


def getIdServiceWWW(x,domain):
    try:
      x.execute("""SELECT `idServiceWWW` FROM `wwwDomainView` WHERE domainName=%s""",[domain])
      results = x.fetchall()

      for row in results:
        bname = row["idServiceWWW"]
	return bname

    except Exception, e:
      print ("Exception:%s" % (e))

def getIdCustomer(x,idServiceWWW):
    try:
      x.execute("""SELECT `idCustomer` FROM `orderView1` WHERE idServiceWWW=%s""",[idServiceWWW])
      results = x.fetchall()

      for row in results:
        bname = row["idCustomer"]
        return bname

    except Exception, e:
      print ("Exception:%s" % (e))


def loadText():
  cmd = '/root/.acme.sh/acme.sh --list'
  fp = os.popen(cmd)
  res = fp.readlines()
  stat = fp.close()
  return res


if __name__ == "__main__": main()
