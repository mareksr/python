def decorator_function(original_function):

    def wrapper_function(*args, **kwargs):
        print('wrapper executed this befor {}'.format(original_function.__name__))
        return original_function(*args, **kwargs) #run

    return wrapper_function

def plum(original_function):

    def wrap(*args, **kwargs):
        print('Plum')
        result = original_function(*args, **kwargs)
        print('Plam')
        return result

    return wrap

class decorator_class(object):
    def __init__(self, original_function):
        self.original_function =  original_function

    def __call__(self, *args, **kwargs):
        print ('call method')
        return self.original_function(*args, **kwargs)

'''
def display():
    print('display function ran')
decorated_display = decorator_function(display)
decorated_display()
'''

# display = decorator_function(display)
@decorator_function
@plum
def display():
    print('display function ran')

#@decorator_class
#def display():
#    print('display function ran')

# display = decorator_function(plum(display))
display()
