#!/usr/bin/python -tt
import re
import sys
import fnmatch
import os
import requests

def getGeoLocation(ip):
  url = 'http://freegeoip.net/json/'+ip
  r = requests.get(url)
  js = r.json()
  return js['country_code']

def getFiles():
  files=[]
  [ files.append(file) for file in os.listdir('/usr/local/assp/logs/') if fnmatch.fnmatch(file, '*bmail*txt') ] 
  return files
        
def getAllLines(files):
  """
    pobiera wszystkie pliki
  """
  allFiles=[]  
  for file in files:
    f=open("/usr/local/assp/logs/"+file,"Ur")
    for line in f:
       allFiles.append(line)
    f.close
  return allFiles

def main():
  lines=getAllLines(getFiles())
  ips={}
  for line in lines:
    match=re.search(r"(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\s<(.*@[\w.]+)>",line)
    if match:
      ip=match.group(1)
      domain=match.group(2)
      if ips.get(ip):
        ips[ip][0]=ips[ip][0]+1
        ips[ip].append(domain)
      else:
      #pierwszy wpis
        ips[ip]=[1,domain]
      #print(match.group(1))

  kot=[ key for key in ips.keys() if ips[key][0]>10 ]
  for i in kot:
    print i
if __name__ == "__main__": main()



