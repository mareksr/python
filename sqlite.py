from sqlalchemy import Column, Integer, Unicode, UnicodeText, String
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from marshmallow import Schema, fields, pprint, post_load


engine = create_engine('sqlite:///lits.db', echo=False)
Base = declarative_base(bind=engine)

class Person(Base):
    __tablename__ = 'Person'
    id = Column(Integer, primary_key=True)
    name = Column(String(32))
    lastname = Column(String(32))

    def __init__(self):
        id = self.id
        name = self.name
        lastname = self.lastname

    def __repr__(self):
        return 'id: {} name: {} lastname: {}'.format(self.id,self.name,self.lastname)

class PersonSchema(Schema):
    id = fields.Integer()
    name = fields.String()
    lastname = fields.String()


def main():
    Base.metadata.bind = engine
    Session = sessionmaker(bind=engine)
    s= Session()
    persons = s.query(Person).all()
    schema = PersonSchema(many=True)
    result = schema.dump(persons)
    print (result.data)

    for person in persons:
      print(person)

if __name__ == '__main__': main()
